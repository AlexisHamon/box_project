#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "datastructure/suffix_tree.h"

void calculate_absent_word(const ST* tree);

#endif
