#ifndef BENCHMARK_H
#define BENCHMARK_H

#include <stdio.h>

#include "fasta.h"


void run_bench(char* filename, char* output_file, int number_of_samples);

#endif
