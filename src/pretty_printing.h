#ifndef PRETTY_PRINTING_H
#define PRETTY_PRINTING_H

#include <bits/types/struct_timeval.h>
#include <time.h>

const char* color(time_t elapsed);

float timedifference_msec(struct timeval t0, struct timeval t1);

void print_file_reading(char* file_name, time_t reading_file_time);

void print_start_tree(int length);

void print_end_tree(int length, time_t building_time);

void print_status(long long unsigned number_left, long long unsigned total, struct timeval start_time);

void print_help();

#endif
