#define _GNU_SOURCE
#include <sys/time.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>

#include "multi_thread_tools.h"

int cpucount(){
    cpu_set_t cpuset;
    sched_getaffinity(0, sizeof(cpuset), &cpuset);
    return CPU_COUNT(&cpuset);
}

JobBuffer* new_job_buff(int threads, char* filename) {
  JobBuffer* job_buff = malloc(sizeof(JobBuffer));
  job_buff->work_to_do = true;
  job_buff->buffer_size = 2 * threads;
  job_buff->sequences = malloc(sizeof(seq*) * job_buff->buffer_size);
  sem_init(&job_buff->s_filled, 0, 0);
  sem_init(&job_buff->s_full, 0, job_buff->buffer_size);
  job_buff->file_name = filename;
  job_buff->clock = 0;
  job_buff->number_of_threads = threads;
  pthread_mutex_init(&job_buff->clock_mutex, NULL);

  job_buff->file_size = 1;
  job_buff->byte_done = 0;
  job_buff->running = true;
  sem_init(&job_buff->s_status, 0, 1);
  gettimeofday(&job_buff->start_time, NULL);

  return  job_buff;
}

