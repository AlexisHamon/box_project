#include <string.h>
#include <time.h>
#include <stdio.h>
#include <sys/time.h>

#include "benchmark.h"
#include "fasta.h"
#include "sequence.h"
#include "datastructure/suffix_tree.h"
#include "datastructure/suffix_tree_construct.h"

#include "pretty_printing.h"
#include "algorithm.h"


void run_bench(char* filename, char* output_file, int number_of_samples) {
  
  FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
  fprintf(gnuplotPipe, "set title \"Benchmark results\"\n");
  fprintf(gnuplotPipe, "set xlabel \"sequence length (in number of chars)\"\n");
  fprintf(gnuplotPipe, "set ylabel \"time (in ms)\"\n");

  FILE* temp;

  if (output_file != NULL) {
    temp = fopen(output_file, "w");
  } else {
    temp = fopen("temp.plot", "w");
  }
  fprintf(temp, "#Benchmark results for the file %s\n#<sequence length(bp)>\t<time to build the suffix tree(ms)>\t<time to compute the absent words(ms)>\t<total time(ms)>\n", filename);


  FASTAFILE* ffp = OpenFASTA(filename);
  seq* seq;
  struct timeval start, building_start, building_end, algorithm_end;
  gettimeofday(&start, NULL);

  unsigned long long bytes_done = 0;

  while ((seq = ReadFASTA(ffp)) != NULL) {

    float build_time = 0;
    float algorithm_time = 0;
    float total_time = 0;
    for (int i = 0; i < number_of_samples; i++) {
    print_status(bytes_done, ffp->size * number_of_samples, start);
      gettimeofday(&building_start, NULL);
      ST* tree = constructST(seq->string, seq->len);
      gettimeofday(&building_end, NULL);
      calculate_absent_word(tree);
      gettimeofday(&algorithm_end, NULL);
      freeST(tree);
      build_time += timedifference_msec(building_start, building_end);
      algorithm_time += timedifference_msec(building_end, algorithm_end);
      total_time += timedifference_msec(building_start, algorithm_end);
      bytes_done += seq->len + strlen(seq->name);
    }

    fprintf(temp, "#%s\n", seq->name);
    fprintf(temp, "%d %f %f %f\n",
            seq->len,
            build_time / number_of_samples,
            algorithm_time / number_of_samples,
            total_time / number_of_samples);
            /* timedifference_msec(building_start, building_end), */
            /* timedifference_msec(building_end, algorithm_end), */
            /* timedifference_msec(building_start, algorithm_end)); */
    /* bytes_done+= seq->len; */


  }
  print_status(1, 1, start);
  printf("\n");

  if (output_file != NULL) {
    fprintf(gnuplotPipe, "plot '%s' u 1:2 smooth unique w linespoints t \"Building tree\", '%s' u 1:3 smooth unique w linespoints t \"Computing absent words\", '%s' u 1:4 smooth unique w linespoints t \"Total\"\n", output_file, output_file, output_file);
  } else {
    fprintf(gnuplotPipe, "plot '%s' u 1:2 smooth unique w linespoints t \"Building tree\", '%s' u 1:3 smooth unique w linespoints t \"Computing absent words\", '%s' u 1:4 smooth unique w linespoints t \"Total\"\n", "temp.plot", "temp.plot", "temp.plot");
  }
  pclose(temp);
  pclose(gnuplotPipe);


  CloseFASTA(ffp);
}
