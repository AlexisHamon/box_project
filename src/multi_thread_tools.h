#ifndef MULTI_THREAD_TOOLS_H
#define MULTI_THREAD_TOOLS_H

#include <bits/types/struct_timeval.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <time.h>
#include <pthread.h>
#include <semaphore.h>
#include <asm-generic/errno-base.h>

#include "sequence.h"


typedef struct JobBuffer {
  sem_t s_filled;
  sem_t s_full;

  int number_of_threads;
  int buffer_size;
  seq** sequences;
  char* file_name;
  bool work_to_do;
  bool running;
  int clock;
  pthread_mutex_t clock_mutex;

  long long unsigned file_size;
  long long unsigned byte_done;
  sem_t s_status;
  struct timeval start_time;

} JobBuffer;

JobBuffer* new_job_buff(int threads, char* filename);

int cpucount();

#endif
