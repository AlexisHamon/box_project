#include "suffix_tree_construct.h"

STiNode makeSTIntermediate(ST* st, STiNode tempstartnode, STiNode startnode, uint i, uint iword, uint i_middle_tempstartnode) {
  STiNode intermediate = makeSTNode(st);
  st->nodes[intermediate/2].i_sta  = i_staGet(st, tempstartnode);
  if (leaf(tempstartnode))
    st->nodes[intermediate/2].i_suf = tempstartnode / 2;
  else
    st->nodes[intermediate/2].i_suf = st->nodes[tempstartnode/2].i_suf;
  //st->nodes[intermediate/2].i_suf  = st->n_leaf;
  i_endSet(st, intermediate, i_middle_tempstartnode);
  
  st->nodes[intermediate/2].childs[(int) u_of_ACTG(st->s[i_middle_tempstartnode])] = tempstartnode;
  i_staSet(st, tempstartnode, i_middle_tempstartnode);
  addChildLeaf(st, intermediate, u_of_ACTG(st->s[i]), i);

  st->nodes[startnode/2].childs[(int) first_u(st, iword)] = intermediate;
  return intermediate;
}

void extendST(ST* st, uint i) {
  STiNode lastnewnode = 0;

  // TYPE A INSERTION LOOP
  for (uint j=st->n_leaf; j<=i; ++j) {
    #ifdef TEST_SUFFIX_TREE_STUFF
    printf("%d<= j=%d <= %d\n", st->n_leaf, j, i);
    #endif
    uint iword = j + st->nodes[st->activenode/2].i_sta - st->nodes[st->activenode/2].i_suf;

    STiNode tempstartnode = st->activenode;
    do {
      st->activenode = tempstartnode;
      iword += st->nodes[st->activenode/2].i_len;
      tempstartnode = child(st, st->activenode, first_u(st, iword));
    } while (tempstartnode && !leaf(tempstartnode) && !(i - iword < st->nodes[tempstartnode/2].i_len));
    
    /* Extension Rule 2 (A new leaf gets created) */
    if (tempstartnode == 0) {
      // On ne peut pas juste ajouter une feuille à une feuille
      // En effet les préfix sont traités par taille décroissante
      addChildLeaf(st, st->activenode, first_u(st, iword), i);
      if (lastnewnode) {
        st->nodes[lastnewnode/2].suffixLink = st->activenode;
        lastnewnode = 0;
      }
    } else { 
      uint i_middle_tempstartnode = i_staGet(st, tempstartnode) + i - iword;

      if (st->s[i_middle_tempstartnode] == st->s[i]) {
        // Extension Rule 3 (current character being processed
        //      is already on the edge)
        
        // if a suffix is in the tree, so it's the case for all of his
        if (lastnewnode)
          st->nodes[lastnewnode/2].suffixLink = st->activenode;
        return;
      }
      // Extension Rule 1 (We need to create an intermediate node
      //      cause a caracter differ)
      STiNode intermediate = makeSTIntermediate(st, tempstartnode, st->activenode, i, iword, i_middle_tempstartnode);

      if (lastnewnode)
        st->nodes[lastnewnode/2].suffixLink = intermediate;
      lastnewnode = intermediate;
    }
    st->activenode = st->nodes[st->activenode/2].suffixLink;
  }
}

ST* constructST(char* s, int len_s) {
  ST* st = allocST(s, len_s);
  makeSTNode(st);
  
  for (uint i=0; i<len_s; ++i) 
    extendST(st, i); 

  st->nodes = realloc(st->nodes, st->n_node * sizeof(STNode));
  st->leaves = realloc(st->leaves, st->n_leaf * sizeof(STLeaf));

  return st;
}

#ifdef TEST_SUFFIX_TREE_STUFF

int main() {
  char s[] = "TATACATTAG";
  ST* st = constructST(s, 10);

  //char s[] = "AATATATT";
  //ST* st = constructST(s, 8);

  //char s[] = "AAAAAAAA";
  //ST* st = constructST(s, 8);

  printSTFile(st, "ST.dot");

  freeST(st);
  return 0;
}

#endif