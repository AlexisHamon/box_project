#ifndef SUFFIX_TREE_CONSTRUCT_H
#define SUFFIX_TREE_CONSTRUCT_H

#include "suffix_tree.h"

ST* constructST(char* s, int len_s);

#endif