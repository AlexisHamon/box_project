#ifndef SUFFIX_TREE_H
#define SUFFIX_TREE_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#define MAX_CHAR 4

typedef uint STiNode;

typedef struct STNode {
  uint i_sta;
  uint i_len;
  uint i_suf;
  STiNode childs[MAX_CHAR];
  uint suffixLink;
} __attribute__((packed)) STNode;

typedef struct STLeaf {
  uint i_sta;
} __attribute__((packed)) STLeaf;

typedef struct ST {
  uint len_s;
  char* s;
  uint n_node;
  uint n_leaf;
  STiNode activenode;
  STNode* nodes;
  STLeaf* leaves;
} ST;


ST* allocST(char* s, uint len_s);

void freeST(ST* st);

void printSTFile(const ST* st, const char* filename);
void printST(const ST* st, FILE* fh);

STiNode makeSTNode(ST* st);

STiNode child(ST* st, STiNode i, char u);

void i_endSet(const ST* st, STiNode i, uint i_end);

void i_staSet(const ST* st, STiNode i, uint i_sta);

STiNode addChildLeaf(ST* st, STiNode i, char u, uint k_sta);

bool leaf(STiNode i);

char first_u(ST* st, uint i);

uint length(const ST* st, STiNode i);

char u_of_ACTG(char c);

uint i_staGet(const ST* st, STiNode i);

#endif
