#include <assert.h>
#include <limits.h>

#include "suffix_tree.h"

#define MIN(a,b) ((a) < (b) ? (a) : (b))

STiNode makeSTNode(ST* st) {
  return st->n_node++ * 2;
}

STiNode makeSTLeaf(ST* st) {
  return st->n_leaf++ * 2 + 1;
}

ST* allocST(char* s, uint len_s) {
  assert(2*len_s < UINT_MAX);
  ST* st = calloc(1, sizeof(ST));
  st->len_s = len_s;
  st->s = s;
  st->nodes = calloc(len_s + 1, sizeof(STNode));
  st->leaves = calloc(len_s + 1, sizeof(STLeaf));
  return st;
}

void freeST(ST* st) {
  free(st->leaves);
  free(st->nodes);
  free(st);
}

char u_of_ACTG(char c) {
  switch (c) {
    case 'A':
    case 'a':
      return (char) 0;
    case 'C':
    case 'c':
      return (char) 1;
    case 'T':
    case 't':
      return (char) 2;
    case 'G':
    case 'g':
      return (char) 3;
    default:
      printf("%c (charcode:%u) is not in {A, T, C, G}\n", c, c);
      assert(false);
  }
}

char ACTG_of_u(char c) {
  switch (c) {
    case 0:
      return 'A';
    case 1:
      return 'C';
    case 2:
      return 'T';
    case 3:
      return 'G';
    default:
      printf("%d is not in {0, 1, 2, 3}\n", c);
      assert(false);
  }
}

uint i_endGet(const ST* st, STiNode i) {
  return st->nodes[i / 2].i_sta+st->nodes[i / 2].i_len;
}

void i_endSet(const ST* st, STiNode i, uint i_end) {
  st->nodes[i / 2].i_len = i_end-st->nodes[i / 2].i_sta;
}

char first_u(ST* st, uint i) {
  return u_of_ACTG(st->s[i]);
}

STiNode child(ST* st, STiNode i, char u) {
  return st->nodes[i / 2].childs[(int) u];
}

STiNode addSTLeaf(ST* st, uint k_sta) {
  STiNode node = makeSTLeaf(st);
  st->leaves[node / 2].i_sta = k_sta;
  return node;
}

STiNode addChildLeaf(ST* st, STiNode i, char u, uint k_sta) {
  STiNode child = addSTLeaf(st, k_sta);
  st->nodes[i / 2].childs[(int) u] = child;
  return child;
}

uint length(const ST* st, STiNode i) {
  if (leaf(i))
    return st->len_s - st->leaves[i / 2].i_sta;
  return st->nodes[i / 2].i_len;
}

bool leaf(STiNode i) {
  return i % 2;
}

uint constructlength(const ST* st, STiNode i, uint k_suf) {
  if (leaf(i))
    return k_suf - st->leaves[i / 2].i_sta;
  return st->nodes[i / 2].i_len;
}

uint i_staGet(const ST* st, STiNode i) {
  if (leaf(i))
    return st->leaves[i / 2].i_sta;
  return st->nodes[i / 2].i_sta;
}

void i_staSet(const ST* st, STiNode i, uint i_sta) {
  if (leaf(i))
    st->leaves[i / 2].i_sta = i_sta;
  else {
    uint i_end = i_endGet(st, i);
    st->nodes[i / 2].i_sta = i_sta;
    i_endSet(st, i, i_end);
  }
}

void printSTNodeLabel(const ST* st, FILE* fh, STiNode i) {
  for (uint k=st->nodes[i/2].i_sta; k<i_endGet(st, i); ++k)
    fprintf(fh, "%c", st->s[k]);
}

void printSTNode(const ST* st, FILE* fh, STiNode i) {
  fprintf(fh, " %d [label=<%d:", i, i);
  printSTNodeLabel(st, fh, i);
  fprintf(fh, "<BR /><FONT POINT-SIZE=\"10\">%d to %d | %d </FONT>>];\n", st->nodes[i/2].i_sta, i_endGet(st, i), st->nodes[i/2].i_suf);
  if ((st->nodes[i/2].suffixLink != 0) || (i == 0))
    fprintf(fh, "%d -> %d [style=dashed color=\"red\"]\n", i, st->nodes[i/2].suffixLink);
  for (uint k=0; k<MAX_CHAR; ++k)
    if (st->nodes[i/2].childs[k] != 0)
      fprintf(fh, " %d -> %d;\n", i, st->nodes[i/2].childs[k]);
}

void printSTLeafLabel(const ST* st, FILE* fh, STiNode i) {
  fprintf(fh, "%c", st->s[st->leaves[i/2].i_sta]);
}

void printSTLeaf(const ST* st, FILE* fh, STiNode i) {
  fprintf(fh, " %d [label=<%d:", i, i);
  printSTLeafLabel(st, fh, i);
  fprintf(fh, "<BR /><FONT POINT-SIZE=\"10\">%d to %d | %d </FONT>>];\n", st->leaves[i/2].i_sta, st->len_s, i/2);
}

void printST(const ST* st, FILE* fh) {
  fprintf(fh, "digraph graphname {\n");
  for (STiNode i=0; i<st->n_node; ++i)
    printSTNode(st, fh, 2*i);
  for (STiNode i=0; i<st->n_leaf; ++i)
    printSTLeaf(st, fh, 2*i+1);
  fprintf(fh, "}\n"); 
}

void printSTFile(const ST* st, const char* filename) {
  FILE* fh = fopen(filename, "w");
  printST(st, fh);
  fclose(fh);
}
