#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <sys/types.h>

typedef struct seq {
    uint len;
    uint maxlen;
    char* name;
    char* string;
    unsigned long skipped_chars;
} seq;

seq* seqAlloc(char* name);

void seqCropAlloc(seq* s);

void seqFree(seq* s);

void seqAdd(seq* s, char c);

#endif
