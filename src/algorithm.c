#include "algorithm.h"
#include "datastructure/suffix_tree_construct.h"

#include <assert.h>

typedef struct absent_word {
    STiNode left_ptr;
    uint offset;
    STiNode righ_ptr;    
} absent_word;


void printSTNodeLabelT2(const ST* st, uint i_suf, uint i_end, uint i_add) {
  for (uint k=i_suf; k<i_end; ++k)
      printf("%c", st->s[k]);
  printf("%c\n", st->s[i_add]);
} 

void printSTNodeLabelT(const ST* st, STiNode left_ptr, uint offset, STiNode righ_ptr) {
  if (leaf(left_ptr)) {
    printSTNodeLabelT2(st, left_ptr/2, i_staGet(st, left_ptr)+offset, i_staGet(st, righ_ptr));
  } else {
    printSTNodeLabelT2(st, st->nodes[left_ptr/2].i_suf, i_staGet(st, left_ptr)+offset, i_staGet(st, righ_ptr));
  }
}

void IterNode(const ST* st, STiNode left_ptr, uint offset, STiNode righ_ptr) {
  if (leaf(righ_ptr))
    return;

  if (leaf(left_ptr) || (st->nodes[left_ptr/2].i_len - offset != st->nodes[righ_ptr/2].i_len)) {
    // left_ptr is bigger than righ_ptr
    // We have one and only child
    uint u = MAX_CHAR;
    if (i_staGet(st, left_ptr) + offset + st->nodes[righ_ptr/2].i_len < st->len_s) {
        u = u_of_ACTG(st->s[i_staGet(st, left_ptr) + offset + st->nodes[righ_ptr/2].i_len]);
        IterNode(st, left_ptr, offset + st->nodes[righ_ptr/2].i_len, 
            st->nodes[righ_ptr/2].childs[u]);
    }
    for (uint k=0; k<MAX_CHAR; ++k) {
      if (k != u && st->nodes[righ_ptr/2].childs[k]) {
        // On a trouvé un mot absent
        #ifdef TEST_ALGORITHM_STUFF
        if (righ_ptr == 0) {
          printSTNodeLabelT(st, left_ptr, 1, st->nodes[righ_ptr/2].childs[k]);
        } else {
          printSTNodeLabelT(st, left_ptr, offset + st->nodes[righ_ptr/2].i_len, st->nodes[righ_ptr/2].childs[k]);
        }
        #endif
      }
    }
  } else {
    // We need to compare left and right childs
    for (uint k=0; k<MAX_CHAR; ++k) {
      if (st->nodes[left_ptr/2].childs[k])
        IterNode(st, st->nodes[left_ptr/2].childs[k], 0, st->nodes[righ_ptr/2].childs[k]);
      else if (st->nodes[righ_ptr/2].childs[k]) {
        // On a trouvé un mot absent
        #ifdef TEST_ALGORITHM_STUFF
        printSTNodeLabelT(st, left_ptr, st->nodes[left_ptr/2].i_len, st->nodes[righ_ptr/2].childs[k]);
        #endif
      }
    }
  }
}


void calculate_absent_word(const ST* tree) {
    #ifdef PARALLEL
    #pragma omp parallel for
    #endif
    for (uint k=0; k<MAX_CHAR; ++k)
        if (tree->nodes[0].childs[k])
            IterNode(tree, tree->nodes[0].childs[k], 1, 0);
}

#ifdef TEST_ALGORITHM_STUFF
int main() {
    char s[] = "TATACATTAG";
    ST* st = constructST(s, 10);
    //char s[] = "AATATATT";
    //ST* st = constructST(s, 8);
    printSTFile(st, "ST.dot");
    
    calculate_absent_word(st);

    freeST(st);
}
#endif

#ifdef TEST_HUMAN_STUFF
#include "fasta.h"

int main(int argc, char** argv) {
    
    FASTAFILE *ffp = OpenFASTA(argv[1]);
    seq* seq;
    while ((seq = ReadFASTA(ffp)) != NULL) {

      ST* st = constructST(seq->string, seq->len);
      calculate_absent_word(st);
      freeST(st); 
      seqFree(seq);
    }
}
#endif