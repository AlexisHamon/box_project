/* fasta.h
 * Declarations for simple FASTA i/o library
 * SRE, Sun Sep  8 05:37:38 2002 [AA2721, transatlantic]
 * CVS $Id$
 */
#ifndef FASTA_H
#define FASTA_H

#include <stdio.h>
#include "sequence.h"

#define FASTA_MAXLINE 512	/* Requires FASTA file lines to be <512 characters */

typedef struct fastafile_s {
  FILE *fp;
  char  buffer[FASTA_MAXLINE];
  size_t size;
} FASTAFILE;

extern FASTAFILE *OpenFASTA(char *seqfile);
extern seq*        ReadFASTA(FASTAFILE *ffp);
extern void       CloseFASTA(FASTAFILE *ffp);
#endif
