#include "datastructure/suffix_tree.h"
#include "datastructure/suffix_tree_construct.h"

#include "fasta.h"



int main(int argc, char** argv) {
  // argv[1] is the name of the fasta file

  FASTAFILE *ffp = OpenFASTA(argv[1]);
  seq* seq = ReadFASTA(ffp);

  ST* tree = constructST(seq->string, seq->len);
  printSTFile(tree, "ST.dot");

  seqFree(seq);
  freeST(tree);
  return 0;
}
