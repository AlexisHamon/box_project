#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <time.h>
#include <sys/time.h>
#include "pretty_printing.h"

const char* green = "\033[1;32m";
const char* orange = "\033[1;33m";
const char* red = "\033[1;31m";
const char* normal = "\033[1;0m";

/* const int BAR_WIDTH = 100; */

int bar_width(){
  struct winsize sz;
  ioctl(0, TIOCGWINSZ, &sz);
  return sz.ws_col - 75;
}

const char* color(time_t elapsed) {
  if (elapsed < 1) {
    return green;
  }
  if (elapsed < 5) {
    return orange;
  }
  return red;
}

float timedifference_msec(struct timeval t0, struct timeval t1) {
  return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

void format_time(char* destination, float time_remaining) {
    unsigned long total_seconds = (unsigned long) (time_remaining / 1000);
    int hours = total_seconds / 3600;
    int minutes = (total_seconds / 60) % 60;
    int seconds = (total_seconds) % 60;
    sprintf(destination, "%02d:%02d:%02d", hours, minutes, seconds);
}


void print_file_reading(char* file_name, time_t reading_file_time) {
  printf("%sFin de la lecture du fichier %s en %ld secondes%s\n",
         green,
         file_name,
         reading_file_time,
         normal);
}

void print_start_tree(int length) {
    printf("Construction d'un arbre de taille %d...\n", length);
}

void print_end_tree(int length, time_t building_time) {
    printf("%sFin de la construction d'un arbre de taille %d en %ld secondes%s\n",
           color(building_time), length, building_time, normal);
}

void print_help() {
  printf("This is a tool to compute minimal absent words from fasta file\n");
  printf("usage : main -f <filename> {flags}\n");
  printf("Flags :\n");
  printf("\t-h, --help\n");
  printf("\t-v, --verbose\n");
  printf("\t-q, --quiet\n");
  printf("\t-t, --threads <int> : choose the number of threads for parralelization\n");
  printf("\t-b, --bench : run benchmarks\n");
  printf("\t-o, --output <filename> : specify the output file for benchmarks\n");

}

void print_status(long long unsigned count, long long unsigned max, struct timeval start_time) {

  float progress = (float) (count) / max;
  int current_bar_width = bar_width();
  int bar_length = progress * current_bar_width;

  struct timeval now;
  gettimeofday(&now, NULL);
  float t = timedifference_msec(start_time, now);

  printf("\rProgress: [");
  for (int i = 0; i < bar_length; ++i) {
    printf("#");
  }
  for (int i = bar_length; i < current_bar_width; ++i) {
    printf(" ");
  }
  printf("] %.2f%%", progress * 100);
  if (count > 0) {
    float time_remaining = ((max - count) * t) / count;
    char result[9];
    format_time(result, time_remaining);
    printf("    Time remaining : %s", result);

  } else {
    printf("    Time remaining : ??:??:??");
  }


  fflush(stdout);
}
