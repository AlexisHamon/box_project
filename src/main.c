#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <asm-generic/errno-base.h>
#include <getopt.h>

#include "sequence.h"
#include "datastructure/suffix_tree.h"
#include "datastructure/suffix_tree_construct.h"

#include "fasta.h"
#include "pretty_printing.h"
#include "multi_thread_tools.h"
#include "algorithm.h"
#include "benchmark.h"

bool VERBOSE = false;
bool QUIET = false;

void* read_fasta_thread(void* arg) {
  JobBuffer* job_buffer = (JobBuffer*) arg;

  FASTAFILE* ffp = OpenFASTA(job_buffer->file_name);
  job_buffer->file_size = ffp->size;
  seq* seq;
  int i = 0;
  while ((seq = ReadFASTA(ffp)) != NULL) {
    sem_wait(&job_buffer->s_full);
    job_buffer->sequences[i] = seq;
    sem_post(&job_buffer->s_filled);
    i = (i + 1) % job_buffer->buffer_size;
  }
  job_buffer->work_to_do = false;

  CloseFASTA(ffp);
  for (int i = 0; i < job_buffer->number_of_threads; i++) {
    sem_post(&job_buffer->s_filled);
  }

  return NULL;
}

void* compute_thread(void* arg) {
  JobBuffer* job_buffer = (JobBuffer*) arg;

  seq* seq;
  int i;
  while (true) {
    if (job_buffer->work_to_do) {
      sem_wait(&job_buffer->s_filled);
    } else {
      if (sem_trywait(&job_buffer->s_filled) == EAGAIN) break;
    }
    pthread_mutex_lock(&job_buffer->clock_mutex);
    i = job_buffer->clock;
    seq = job_buffer->sequences[i];
    job_buffer->sequences[i] = NULL;
    job_buffer->clock = (i + 1) % job_buffer->buffer_size;
    pthread_mutex_unlock(&job_buffer->clock_mutex);
    if (seq == NULL) break;

    ST* tree = constructST(seq->string, seq->len);
    calculate_absent_word(tree);
    freeST(tree);
    job_buffer->byte_done += seq->len + strlen(seq->name) + seq->skipped_chars;
    sem_post(&job_buffer->s_status);

    seqFree(seq);
    sem_post(&job_buffer->s_full);
  }

  return NULL;
}

void* status_thread_fun(void* arg) {
  JobBuffer* job_buffer = (JobBuffer*) arg;
  while (job_buffer->running) {
    sem_wait(&job_buffer->s_status);
    print_status(job_buffer->byte_done, job_buffer->file_size, job_buffer->start_time);
  }
  print_status(1, 1, job_buffer->start_time);
  printf("\n");

  return NULL;
}


int main(int argc, char** argv) {

  int number_of_cpu = cpucount();

  int opt;
  int option_index = 0;
  char *filename = NULL;
  char *output = NULL;
  bool bench = false;
  int number_of_samples;

  static struct option long_options[] = {
    {"file", required_argument, 0, 'f'},
    {"threads", required_argument, 0, 't'},
    {"VERBOSE	", no_argument, 0, 'v'},
    {"quiet", no_argument, 0, 'q'},
    {"help", no_argument, 0, 'h'},
    {"bench", optional_argument, 0, 'b'},
    {"output", required_argument, 0, 'o'},
    {0, 0, 0, 0} // Indicates the end of options
  };

  // Process command line options
  while ((opt = getopt_long(argc, argv, "o:b::hqvf:t:", long_options, &option_index)) != -1) {
    switch (opt) {
      case 'f':
        // optarg contains the filename provided after -f
        filename = optarg;
        break;

      case 'v':
        // Set the VERBOSE	 flag
        VERBOSE	 = true;
        break;

      case 'q':
        QUIET = true;
        break;

      case 'h':
        print_help();
        return 0;

      case 't':
        number_of_cpu = atoi(optarg);
        break;

      case 'b':
        bench = true;
        if (optarg == NULL && optind < argc && argv[optind][0] != '-') {
          number_of_samples = atoi(argv[optind]);
        } else {
          number_of_samples = 1;
        }
        break;

      case 'o':
        output = optarg;
        break;

      case '?':
        // Invalid option or missing argument
        print_help();
        return 1;

      default:
        break;
    }
  }
  if (filename == NULL) {
    fprintf(stderr, "No file given for input data, exiting...\n");
    print_help();
    exit(1);
  }

  if (bench) {
    run_bench(filename, output, number_of_samples);
    exit(0);
  }

  JobBuffer* job_buff = new_job_buff(number_of_cpu, filename);

  pthread_t reading_thread;
  pthread_t status_thread;

  time_t start_time = time(NULL);

  pthread_create(&reading_thread, NULL, (void*)read_fasta_thread, (void*)job_buff);

  pthread_t* building_threads = malloc(sizeof(pthread_t) * number_of_cpu);

  for (int i = 0; i < number_of_cpu; i++) {
    pthread_create(&building_threads[i], NULL, (void *)compute_thread, (void*)job_buff);
  }
  if (!QUIET) pthread_create(&status_thread, NULL, (void*)status_thread_fun, (void*)job_buff);


  pthread_join(reading_thread, NULL);

  for (int i = 0; i < number_of_cpu; i++) {
    pthread_join(building_threads[i], NULL);
  }
  job_buff->running = false;

  if (!QUIET) {
    sem_post(&job_buff->s_status);
    pthread_join(status_thread, NULL);

    time_t total_time = time(NULL) - start_time;
    printf("End of building of evry trees. Took %ld seconds\n", total_time);
  }


  free(building_threads);

  return 0;
}
