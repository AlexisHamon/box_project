
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "sequence.h"

bool is_ACTG(char s) {
  switch (s) {
    case 'A':
    case 'C':
    case 'T':
    case 'G':
    case 'a':
    case 'c':
    case 't':
    case 'g':
      return true;
    default:
      return false;
  }
}

seq* seqAlloc(char* name) {
    seq* s = malloc(sizeof(seq));
    s->name = malloc((strlen(name)+1) * sizeof(char));
    strcpy(s->name, name);
    s->string = malloc(128 * sizeof(char));
    s->maxlen = 128;
    s->len = 0;
    s->skipped_chars = 0;
    return s;
}

void seqCropAlloc(seq* s) {
  s->maxlen = s->len;
  s->string = realloc(s->string, s->maxlen * sizeof(char));
}

void seqFree(seq* s) {
    free(s->string);
    free(s->name);
    free(s);
}

void seqAdd(seq* s, char c) {
    if (!is_ACTG(c)) {
      s->skipped_chars++;
      return;
    }
    
    if (s->len == s->maxlen) {
        s->maxlen *= 2;
        s->string = realloc(s->string, s->maxlen * sizeof(char));
    }

    s->string[s->len] = toupper(c);
	s->len++;
}
