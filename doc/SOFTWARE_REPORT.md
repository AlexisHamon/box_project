# Software report

All our project has been made in C for both time and space performances. There are no other dependencies than the standard library.

All the source code is under the `src` folder.

The most important files are :
* suffix\_tree.c, our implementation of suffix trees
* algorithm.c, the actual algorithm that computes MAWs

The other files mostly are for test and benchmark purposes.

## Reading the FASTA File

The code from `fasta.c` was mostly inspired from [https://github.com/eturro/mmseq/blob/master/src/fasta.c](https://github.com/eturro/mmseq/blob/master/src/fasta.c) but we modified the parsing core since :
* the allocation size are growing exponentially to have the correct size faster.
* we create a new struct call `seq` as in the file `sequence.c`

We know this could be improved, and possibly be much faster with the `mmap` function, but improving this wasn't our priority.

## Building the suffix trees

The building of a suffix trees is done in the file `suffix_tree.c`. We implement a version of the Ukkonen algorithm, including suffix links.

A suffix tree is an element of the struct `ST`.

We start by allocating a majoration of the space needed for our tree : the number of nodes is smaller than 2 * the sequence length.

In fact we separate the leaves from intermediate nodes since we only needs to store one integer per leaf which also avoid the children array of the intermediates nodes to save memory space.
Odd nodes are leaves while even odds are intermediaries.

For debug purposes we can output our trees as graphviz files (.dot) with the `printSTFile` function. These files can be oppened by xdot or convert into svg/png with the dot tool. 

After the tree is built, we do a realloc to free the unused allocated space (we save around 15% of memory).


## Traversal of the tree for our algorithm

The traversal of our suffix trees is done through the function `IterNode` in `algorithm.c`.

The current function is a simple transcription of our algorithm using recursive calls to `IterNode`. We did try a non-recursive function, better in theory for the stack (so an improvment of time and memory usage), but we did not see significant result, and decided to stick to the more readable recursive function.


## Multithreading

To improve performances, we made all our code with multithreading in mind. The various function to achieve this are in the file `multi_thread_tools.c`.


Basically, we implement a circular buffer that will contains the sequences to work on. The size of the buffer is two times the number of threads. One reading thread will read the fasta file and fill the buffer, while computing threads will read the circular buffer and start working on building the suffix trees and executing the traversal algorithm.

The circular buffer is protected by two semaphores (one that tells if it has elements to work on, and one that tells if it is full), and one mutex for the index of the next job to get.

The `s_print` semaphore is only needed to print the status bar when a job is finished (otherwise we would use our precious time printing the same inforamtion again and again to stdout).

By defaut, the programm will spawn a number of thread that is equal to the number of core present on your computer. This is the fastest configuration, but you can easily choose a number of thread with the `-t` flag.

## Printing the output

The main branch currently does not output anything. We focused on the more interesting part. However, the branch `monocore`, used to compare with us with the implementation found here : 
[https://github.com/solonas13/maw/tree/master](github.com/solonas13/maw/tree/master) and so print the output in `output_file` the same way as they do. The outputting of our project will be improved, using better multithreading and the `mmap` function.

## To be improved

Many things are to be improved. Our multithreading scheme could incorporate iteration of the `IterNode` function, which will enable parallelization of the traversal of the suffix tree. This is especially useful when working on a single, very long sequences. However, the building of the tree doesn't seem to be parallelizable.

We will want to use the `mmap` function to print fast and in parrallel the output.

We want to store the characters of the sequences with only 2 bits (as there are only 4 cases possible), and so technically, store 4 letters in one `char`.

