default:
	cmake -S src -B src 
	make -C src main
	mv src/main main

build:
	cmake -S src -B src 
	make -C src
.PHONY : build

run: build
	./src/box
.PHONY : run

valgrind: build
	valgrind ./src/box
.PHONY : valgrind

dot:
	xdot ST.dot

svg:
	dot -T svg ST.dot -o ST.svg
	inkview ST.svg
.PHONY : svg
