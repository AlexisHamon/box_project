# BOX project

This git repository countains our work for the BOX project. You can find the final report and the software report under the `doc` folder.

## Installation

To build the project, you will nee to have a C compiler (like `gcc`), and `cmake` installed. There are no other dependencies, but you will need `gnuplot` to run the benchmarks.

To compile the project, just
```
make
```


## Usage

To use the programm, run :

```
./main -f <input_file> [ flags ]
```

| Flag | Abreviation | Function |
|------|-------------|----------|
| `--help` | `-h` | Print help |
| `--file <path>` | `-f <path>` | Give path to the input file (mandatory) |
| `--output <path>` | `-o <path>` | Give path to the input file (currently only for benchmarks) |
| `--threads <n>` | `-t <n>` | Specify the number of threads to spawn (the default is the number of core on your CPU)|
| `--bench` | `-b` | Compute benchmarks |
| `--verbose` | `-v` | Print more information to stdout |
| `--quiet` | `-q` | Print nothing to stdout |

## Benchmarks

You can run benchmarks using the command
```
./main -f <input_file> -o <output_file> -b
```
If `<output_file>` is not specified, the results will be put into `./temp.plot`.

This will print benchmarks in the output file, and plot them using gnuplot. The output will look like this :
```
#Name of the DNA sequence
<lenth of the sequence> <time to build the suffix tree> <time to compute the absent words> <total time>
```

If you want to replot the values stored in the file `<file>`, start gnuplot and run
```
plot '<file>' u 1:2 smooth unique w linespoints t "Building tree", '<file>' u 1:3 smooth unique w linespoints t "Computing absent words", '<file>' u 1:4 smooth unique w linespoints t "Total"
```
